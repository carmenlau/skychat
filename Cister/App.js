/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  FlatList,
} from 'react-native';
import skygear from 'skygear/react-native';
import skygearChat from 'skygear-chat';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {
constructor(props){
  super(props);
  this.state = {
    conversation:'',
    text:'',
    data:'',
    disabled:'disabled'
  }
}
//登入
  APPClick(){
    skygear.config({
      'endPoint': 'https://cfdcome.skygeario.com/', // trailing slash is required
      'apiKey': '6c90577de2894e1bafd126608476bb8f',
    }).then(() => {
      this.APPClick4()
        console.log('skygear is ready for making API call');
        return skygear.auth.loginWithUsername('zhangsong','1234');
        // skygear.auth.signupWithUsername('zhangsong', '1234')
        // .then((res) => {
        //   console.log(res); // user record
        //   // console.log(res["username"]); // username of the user
        // });
    }).then((user) => {
        console.log(user);
    }).catch ((err) => {
        console.error(err);
    });
  }
//创建会话
  APPClick2(){
    skygear.config({
      'endPoint': 'https://cfdcome.skygeario.com/', // trailing slash is required
      'apiKey': '6c90577de2894e1bafd126608476bb8f',
    }).then(() => {
    skygearChat.createDirectConversation('85a65ef9-271e-40f1-8307-b4ca76699bc3', '"zhangsongw')
    .then((conversation)=> {
      // a new direct conversation is created
      this.setState({
        conversation
      })
      console.log('Conversation created!', conversation);
    }, (err)=> {
      console.log('Failed to create conversation');
    });
  });
  }
  //发送
  APPClick3(){
    if(!this.state.conversation) return false;
    if(!this.state.text) return false;
    const message = this.state.text;
    const metadata = {
      textColor: 'red',
      isImportant: true
  };
  let sfet = this;
    skygearChat.createMessage(this.state.conversation , message, metadata, null, null)
      .then(function (result) {
        console.log('Message sent!', result.body);
        sfet.setState({text:''});
        sfet.APPClick7();
        
    });
  }
  //获取聊天室
  APPClick4(){
    skygear.config({
      'endPoint': 'https://cfdcome.skygeario.com/', // trailing slash is required
      'apiKey': '6c90577de2894e1bafd126608476bb8f',
    }).then(() => {
      let sfet = this;
      skygearChat.getConversations()
      .then(function (conversations){
        console.log(conversations);
        sfet.setState({
          conversation:conversations[0]
        })
        sfet.APPClick7();
      });
  });
    
  }
  APPClick5(){
    // skygearChat.subscribe(function handler(msgData) {
    //   // Handle Message data here
    //   // messages.push();
    //   console.log('msgData',msgData)
    //   // messages.push(msgData.record);
      
    // });
    skygearChat.addParticipants(this.state.conversation,'85a65ef9-271e-40f1-8307-b4ca76699bc3')
      .then(function (conversation) {
        console.log('加入用户成功=======',conversation);
      })
      .catch((err)=>{
        console.log('err',err)
      })
  }
  //****刷新****
  APPClick6(){
  //   skygearChat.updateConversation(this.state.conversation, "new title", null)
  // .then(function(conversation) {
  //   console.log('Conversation updated',conversation);
  // }, function (err) {
  //   console.log('Failed to update the conversation');
  // });
  const message = "Hi! This is a message with assets";
  // Getting the asset file using jQuery
  // const assets = $('message-asset').files[0];

  skygearChat.createMessage(this.state.sconversation, message, null, null)
    .then(function (result) {
      console.log('Message sent!', result);
  });
  }
  //获取会话记录
  APPClick7(){
    const limit=10;
    const beforeTime = new Date();
    let sfet = this;
    skygear.config({
      'endPoint': 'https://cfdcome.skygeario.com/', // trailing slash is required
      'apiKey': '6c90577de2894e1bafd126608476bb8f',
    }).then(() => {
      skygearChat.getMessages(this.state.conversation, limit, beforeTime)
      .then(function (messages) {
        console.log(messages)
        let data = [];
        messages.map((val,ind)=>{data.push(val)});
        sfet.setState({data})
      });
    });
  }
  _renderItem=(item)=>{
    console.log(item)
    var txt = item.item.createdBy+'-----' + item.item.body;
    var bgColor = item.index % 2 == 0 ? '#a1caf1' : '#7fffd4';
    return <View ><Text style={[{flex:1,height:100,backgroundColor:bgColor}]}>{txt}</Text></View>
  }
  render() {
    return (
      <View>
        {this.state.conversation ?
      <View style={{flexDirection:'row'}}>
        <TextInput
        style={{width:300}}
        placeholder="輸入"
        onChangeText={(text) => this.setState({text})}
        value={this.state.text}>
        </TextInput>
        {this.state.text ?  <Button
        color="#841584"
        onPress={() =>this.APPClick3()}
        title="发送"
      />
      : null}
      </View>
      : null}
      <View>
      <Button onPress={() =>this.APPClick()} title="登入"/>
      <Button
        onPress={() =>this.APPClick2()}
        title="创建会话"
      />
      {/* <Button onPress={() =>this.APPClick3()} title="发送" />  */}
      <Button
        onPress={() =>this.APPClick4()}
        title="获取聊天室"
      />
      <Button
        onPress={() =>this.APPClick5()}
        title="添加熊峰"
      />
      <Button
        onPress={() =>this.APPClick6()}
        title="****刷新****"
      />
       <Button
        onPress={() =>this.APPClick7()}
        title="获取会话记录"
      />
        </View>
      
      <View>
          <FlatList
              // onRefresh={this._refreshing}
              // onEndReached={this._onload}
              // onEndReachedThreshold={0}
              // refreshing={false}
              data={this.state.data}
              renderItem={this._renderItem}
          />
      </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
